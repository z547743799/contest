call plug#begin('~/.vim/plugged')

Plug 'Valloric/YouCompleteMe'
Plug 'fatih/vim-go'
Plug 'scrooloose/nerdtree' "树
Plug 'jistr/vim-nerdtree-tabs' "树
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'nsf/gocode', { 'rtp': 'vim', 'do': '~/.vim/plugged/gocode/vim/symlink.sh' }
Plug 'Manishearth/godef'
Plug 'powerline/powerline'
Plug '~/.fzf'  "查找
Plug 'junegunn/vim-easy-align'



call plug#end()
let g:ycm_server_python_interpreter='/usr/bin/python2'
let g:ycm_global_ycm_extra_conf='~/.vim/.ycm_extra_conf.py'
map <C-n> :NERDTreeToggle<CR>
let NERDTreeMinimalUI = 1
let NERDTreeShowHidden = 1
" 总是显示状态栏
 set laststatus=2
" " 显示光标当前位置
 set ruler
" " 开启行号显示
 set number
 " 高亮显示当前行/列
 set cursorline
 "set cursorcolumn
" " 高亮显示搜索结果
 set hlsearch
set tags=~/path/tags
"以可视模式启动交互式EasyAlign（例如vipga）
xmap ga <Plug>（EasyAlign）

"为运动/文本对象启动交互式EasyAlign（例如，gaip）
nmap ga <Plug>（EasyAlign）

let g:nerdtree_tabs_open_on_console_startup = 1
let g:nerdtree_tabs_focus_on_files = 1

let g:NERDTreeIndicatorMapCustom = {
    \ "Modified"  : "✹",
    \ "Staged"    : "✚",
    \ "Untracked" : "✭",
    \ "Renamed"   : "➜",
    \ "Unmerged"  : "═",
    \ "Deleted"   : "✖",
    \ "Dirty"     : "✗",
    \ "Clean"     : "✔︎",
    \ 'Ignored'   : '☒',
    \ "Unknown"   : "?"
    \ }
