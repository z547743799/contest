# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/lzw/.vim/plugged/YouCompleteMe/third_party/ycmd/cpp/ycm/Candidate.cpp" "/home/lzw/context/vim-ide/ycm/CMakeFiles/ycm_core.dir/Candidate.cpp.o"
  "/home/lzw/.vim/plugged/YouCompleteMe/third_party/ycmd/cpp/ycm/CandidateRepository.cpp" "/home/lzw/context/vim-ide/ycm/CMakeFiles/ycm_core.dir/CandidateRepository.cpp.o"
  "/home/lzw/.vim/plugged/YouCompleteMe/third_party/ycmd/cpp/ycm/Character.cpp" "/home/lzw/context/vim-ide/ycm/CMakeFiles/ycm_core.dir/Character.cpp.o"
  "/home/lzw/.vim/plugged/YouCompleteMe/third_party/ycmd/cpp/ycm/CharacterRepository.cpp" "/home/lzw/context/vim-ide/ycm/CMakeFiles/ycm_core.dir/CharacterRepository.cpp.o"
  "/home/lzw/.vim/plugged/YouCompleteMe/third_party/ycmd/cpp/ycm/CodePoint.cpp" "/home/lzw/context/vim-ide/ycm/CMakeFiles/ycm_core.dir/CodePoint.cpp.o"
  "/home/lzw/.vim/plugged/YouCompleteMe/third_party/ycmd/cpp/ycm/CodePointRepository.cpp" "/home/lzw/context/vim-ide/ycm/CMakeFiles/ycm_core.dir/CodePointRepository.cpp.o"
  "/home/lzw/.vim/plugged/YouCompleteMe/third_party/ycmd/cpp/ycm/IdentifierCompleter.cpp" "/home/lzw/context/vim-ide/ycm/CMakeFiles/ycm_core.dir/IdentifierCompleter.cpp.o"
  "/home/lzw/.vim/plugged/YouCompleteMe/third_party/ycmd/cpp/ycm/IdentifierDatabase.cpp" "/home/lzw/context/vim-ide/ycm/CMakeFiles/ycm_core.dir/IdentifierDatabase.cpp.o"
  "/home/lzw/.vim/plugged/YouCompleteMe/third_party/ycmd/cpp/ycm/IdentifierUtils.cpp" "/home/lzw/context/vim-ide/ycm/CMakeFiles/ycm_core.dir/IdentifierUtils.cpp.o"
  "/home/lzw/.vim/plugged/YouCompleteMe/third_party/ycmd/cpp/ycm/PythonSupport.cpp" "/home/lzw/context/vim-ide/ycm/CMakeFiles/ycm_core.dir/PythonSupport.cpp.o"
  "/home/lzw/.vim/plugged/YouCompleteMe/third_party/ycmd/cpp/ycm/Result.cpp" "/home/lzw/context/vim-ide/ycm/CMakeFiles/ycm_core.dir/Result.cpp.o"
  "/home/lzw/.vim/plugged/YouCompleteMe/third_party/ycmd/cpp/ycm/Utils.cpp" "/home/lzw/context/vim-ide/ycm/CMakeFiles/ycm_core.dir/Utils.cpp.o"
  "/home/lzw/.vim/plugged/YouCompleteMe/third_party/ycmd/cpp/ycm/Word.cpp" "/home/lzw/context/vim-ide/ycm/CMakeFiles/ycm_core.dir/Word.cpp.o"
  "/home/lzw/.vim/plugged/YouCompleteMe/third_party/ycmd/cpp/ycm/versioning.cpp" "/home/lzw/context/vim-ide/ycm/CMakeFiles/ycm_core.dir/versioning.cpp.o"
  "/home/lzw/.vim/plugged/YouCompleteMe/third_party/ycmd/cpp/ycm/ycm_core.cpp" "/home/lzw/context/vim-ide/ycm/CMakeFiles/ycm_core.dir/ycm_core.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "YCMD_CORE_VERSION=37"
  "YCM_EXPORT="
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/lzw/.vim/plugged/YouCompleteMe/third_party/ycmd/cpp/pybind11"
  "/usr/include/python2.7"
  "/home/lzw/.vim/plugged/YouCompleteMe/third_party/ycmd/cpp/llvm/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
