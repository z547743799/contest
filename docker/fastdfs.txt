使用Docker安装FastDFS

获取镜像
可以利用已有的FastDFS Docker镜像来运行FastDFS。
获取镜像可以通过下载

docker image pull delron/fastdfs
也可是直接使用提供给大家的镜像备份文件

docker load -i 文件路径/fastdfs_docker.tar
加载好镜像后，就可以开启运行FastDFS的tracker和storage了。

运行tracker
执行如下命令开启tracker 服务
docker run -dti --network=host --name tracker -v /var/fdfs/tracker:/var/fdfs delron/fastdfs tracker
我们将fastDFS tracker运行目录映射到本机的 /var/fdfs/tracker目录中。
执行如下命令查看tracker是否运行起来

docker container ls
如果想停止tracker服务，可以执行如下命令

docker container stop tracker
停止后，重新运行tracker，可以执行如下命令

docker container start tracker

运行storage
执行如下命令开启storage服务
docker run -dti --network=host --name storage -e TRACKER_SERVER=10.211.55.5:22122 -v /var/fdfs/storage:/var/fdfs delron/fastdfs storage
TRACKER_SERVER=本机的ip地址:22122 本机ip地址不要使用127.0.0.1
我们将fastDFS storage运行目录映射到本机的/var/fdfs/storage目录中
执行如下命令查看storage是否运行起来

docker container ls
如果想停止storage服务，可以执行如下命令

docker container stop storage
停止后，重新运行storage，可以执行如下命令

docker container start storage
注意：如果无法重新运行，可以删除/var/fdfs/storage/data目录下的fdfs_storaged.pid 文件，然后重新运行storage。
